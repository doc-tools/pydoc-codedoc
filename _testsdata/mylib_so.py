# fake .so module for unittests


class _pybind_enum_value:
    """Simulate the pybind11 value in enums"""

    def __init__(self, name, value):
        self.name = name
        self.value = value


def simple(a, b):
    """Simple function.

    Args:
        a (int): first argument
        b (float): second argument
    """
    return a * b


class PythonBool:
    """Bool that may be *None*.

    Attributes:
        FALSE (int): False is 0
        NONE (int): None means undefined, is -1
        TRUE (int): True is 1
    """

    FALSE = _pybind_enum_value("FALSE", 0)
    NONE = _pybind_enum_value("NON", -1)
    TRUE = _pybind_enum_value("TRUE", 1)

    ignored = 0

    @property
    def name(self):
        """name(self: handle) -> str"""

    @property
    def value(self):
        return 2

# *pydoc_codedoc* - Extension of *pydoc* that produces Python code

> Note:
> *pydoc_codedoc* is still in beta state.

## Usage

```shell
./pydoc_codedoc.py mymodule.objectname
```

## A companion of ReadTheDocs for Python extensions

This tool has been started to publish a Sphinx documentation of *code_aster*
([web][web] / [repository][src])
which is a software for finite element analysis and numeric simulation
in structural mechanics with a large range of applications.
It is written in C, C++ and Fortran with a Python extension exported using [pybind11][pyb]
(+ Python modules).

It is not reasonnable to automatically publish its Sphinx documentation
on [ReadTheDocs][rtd] by setting up all the prerequisites and compiling more than
one million of lines of code to build its `libaster.so`
(link to [code_aster repository][src]).

Here comes *pydoc_codedoc* that creates a Python file from the compiled extension.
This Python module can be commited into the repository and "statically" used
on ReadTheDocs.

## Based on *pydoc*

[pydoc][pydoc] is a module from the Python Standard Library.
It generates documentation from Python modules.
It provides a text output of the documentation (the `TextDoc` renderer) and
can also create or serve html pages (the `HTMLDoc` renderer).

*pydoc_codedoc* just adds another renderer, named `CodeDoc`, that produces
Python code. It is very similar to `TextDoc`, uses the *pydoc* functions
to inspect objects and to extract arguments specs and docstrings.

## Useless example

*pydoc_codedoc* does not matter if the module is compiled or pure-Python.

```shell
./pydoc_codedoc pydoc_codedoc.CodeDoc
```

produces:

```python
class CodeDoc(pydoc.TextDoc):
    """Subclass `pydoc.TextDoc` renderer to generate equivalent Python code
    (only signature and docstring) from pybind11 objects.

    This is useful to publish use Sphinx autodoc capabilities on static Python
    files, without compiling huge pybind11 extension.
    """

    def bold(self, text):
        """Format a string in bold by overstriking.
        """

    def docclass(self, object, name=None, mod=None, *ignored):
        """Produce text documentation for a given class object.
        """

    def docdata(self, object, name=None, mod=None, cl=None):
        """Produce text documentation for a data descriptor.
        """

    def docroutine(self, object, name=None, mod=None, cl=None):
        """Produce text documentation for a function or method object.
        """
```

## Example of use for code_aster

*pydoc_codedoc* is used to create [libaster.py][libasterpy] from its dynamic library.
This object is originally defined in C++, its docstrings are extracted by
*pydoc_codedoc* to produce this [DataStructure documentation][baseds].
Its full documentation is published on [ReadTheDocs][doc].

With a compiled version of code_aster and `PYTHONPATH`/`LD_LIBRARY_PATH` set to
import `libaster.so`:

```shell
./pydoc_codedoc libaster.DataStructure > output.py
```

`output.py` starts with:

```python
# class DataStructure in libaster

class DataStructure:
    pass

    # Method resolution order:
    #     DataStructure
    #     pybind11_builtins.pybind11_object
    #     builtins.object

    # Methods defined here:

    def __init__(self,  *args, **kwargs):
        """Initialize self.  See help(type(self)) for accurate signature.
        """

    def addDependency(self, ds):
        """Add a dependency to a *DataStructure*.

        Arguments:
            ds (*DataStructure*): Parent *DataStructure* to depend on.
        """
[...]
```

## Limitations

As previously said, *pydoc_codedoc* project has been started for a specific need.
For example, the current usage is to call the `get_python_code()` function
on the objects to be documented.

It has been tested with Python 3.6 to Python 3.9.
It may need an update for each new version of the [pydoc][pydoc] module.

When it will be completed, it could be proposed for [pydoc][pydoc].

## Contribution

Contributions are encouraged to complete the possibilities.
See [CONTRIBUTING](CONTRIBUTING.md).

[src]: https://gitlab.com/codeaster/src
[libasterpy]: https://gitlab.com/codeaster/src/-/blob/main/doc/_automatic_/libaster.py
[web]: https://code-aster.org
[doc]: https://codeaster.readthedocs.io
[baseds]: https://codeaster.readthedocs.io/en/latest/_automatic_/objects_DataStructure.html#datastructure-object
[rtd]: https://readthedocs.org/
[pyb]: https://github.com/pybind/pybind11
[pydoc]: https://docs.python.org/3/library/pydoc.html

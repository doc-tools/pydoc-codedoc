#!/usr/bin/env python3

"""Unittests for pydoc-codedoc.

This module is a part of the pydoc-codedoc project.
"""

# pydoc_codedoc Copyright 2022 Mathieu Courtois <mathieu.courtois.at.gmail.com>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

# Python and pydoc are provided under the terms of the Python License, Version 2,
# Copyright (c) 2001, 2002, 2003, 2004, 2005, 2006 Python Software Foundation.

import sys
import unittest

sys.path.append(".")
sys.path.append("_testsdata")
from pydoc_codedoc import _remove_enumlike, get_python_code, pybind_args


class ExtractArgsTest(unittest.TestCase):
    """pybind specific syntax for arguments."""

    def test0_default(self):
        argstr = "func(self: libaster.SimpleFieldOnNodesReal, copy: bool = False) -> float"
        args, doc = pybind_args("func", argstr)
        self.assertEqual(args, "(self, copy=False)")
        self.assertEqual(doc, "")

        argstr = "func(self: libaster.DataStructure, unit: int = 6) -> None"
        args, doc = pybind_args("func", argstr)
        self.assertEqual(args, "(self, unit=6)")
        self.assertEqual(doc, "")

        argstr = "func(self: libaster.DataStructure, values: int = []) -> None"
        args, doc = pybind_args("func", argstr)
        self.assertEqual(args, "(self, values=[])")
        self.assertEqual(doc, "")

        argstr = "func(self: libaster.DataStructure, values: int = [0, 1]) -> None"
        args, doc = pybind_args("func", argstr)
        self.assertEqual(args, "(self, values=[0, 1])")
        self.assertEqual(doc, "")

    def test1_object(self):
        argstr = (
            "addDirichletBC(self: libaster.BaseAssemblyMatrix, currentLoad: DirichletBC, "
            "func: libaster.Function = <libaster.Function object at 0x560f4cfbc7e0>) -> None"
        )
        args, doc = pybind_args("addDirichletBC", argstr)
        self.assertEqual(args, "(self, currentLoad, func)")
        self.assertEqual(doc, "")

    def test2_pair(self):
        argstr = "func(self: libaster.SimpleFieldOnNodesReal, arg0: Tuple[int, int]) -> float"
        args, doc = pybind_args("func", argstr)
        self.assertEqual(args, "(self, arg0)")
        self.assertEqual(doc, "")

    def test3_varargs(self):
        argstr = "func(self: libaster.Formula, *val: List[float]) -> None"
        args, doc = pybind_args("func", argstr)
        self.assertEqual(args, "(self, *val)")
        self.assertEqual(doc, "")

    def test4_enum(self):
        decl = (
            "a: int = <PythonBool.NONE: -1>, b: double = <TypeDouble: 3.14159>, "
            "c: std::string = <TypeStr: 'hello'>"
        )
        args = _remove_enumlike(decl)
        self.assertEqual(args, "a: int = -1, b: double = 3.14159, c: std::string = 'hello'")

    def test5_enumargs(self):
        argstr = (
            "getNodes(self: libaster.Mesh, group_name: List[str] = [], "
            "localNumbering: bool = True, same_rank: int = <PythonBool.NONE: -1>, "
            "test: string = <TypeStr: 'a string'>) -> List[int]"
        )
        args, doc = pybind_args("getNodes", argstr)
        self.assertEqual(
            args, "(self, group_name=[], localNumbering=True, same_rank=-1, test='a string')"
        )
        self.assertEqual(doc, "")


class DocTest(unittest.TestCase):
    """some specific cases."""

    @staticmethod
    def _split(text):
        return "\n".join([i.rstrip() for i in text.strip().splitlines()])

    def test0_basic(self):
        import mylib_so

        refe = '''
# function simple in mylib_so

def simple(a, b):
    """Simple function.

    Args:
        a (int): first argument
        b (float): second argument
    """
'''
        doc = get_python_code("mylib_so.simple")
        self.assertEqual(self._split(doc), self._split(refe))

        doc = get_python_code("mylib_so.PythonBool")
        # print(doc)
        self.assertTrue("None means undefined" in doc)
        self.assertTrue("TRUE = 1" in doc)
        self.assertFalse("ignored = 0" in doc)
        self.assertEqual(doc.count("@property"), 2)


if __name__ == "__main__":
    unittest.main()
